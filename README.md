# NordCodeChallenge

This is a repository contains a code challenge for Nord Security

## Getting started

Open the project and run. CocoaPods is not integrated. 
The app run for both iOS and macOS

## Stack
- Swift 5.5 Xcode 13.2.^
- SPM (Swift Package Manager)
- SwiftUI
- Combine
- CoreData

## Architecture:
- Modulrized applecation
- VIPER
- Factory Pattern (to incapsolate each module)
- Depandancy Injection Contaner
