import Combine
import SwiftUI

public protocol ServerListInteractorProtocol {
    func fetchServers(token: String) -> AnyPublisher<[Server], NetworkError>
    func loadServers() -> AnyPublisher<[Server], Error>
    func replaceServers(_ servers: [Server]) 
}

final class ServerListInteractor: ServerListInteractorProtocol {
    private let networkService: NetworkServiceProtocol
    private let storage: ServerResponseStorage
    
    init(
        networkService: NetworkServiceProtocol,
        storage: ServerResponseStorage
    ) {
        self.networkService = networkService
        self.storage = storage
    }
    
    func fetchServers(token: String) -> AnyPublisher<[Server], NetworkError> {
        networkService.fetchServers(token: token)
            .eraseToAnyPublisher()
    }
    
    func loadServers() -> AnyPublisher<[Server], Error> {
        storage.fetch()
            .eraseToAnyPublisher()
    }
    
    func replaceServers(_ servers: [Server]) {
        _ = storage.clear()
            .sink { result in
                switch result {
                case .failure(let error):
                    debugPrint(error)
                case .finished: break
                }
            } receiveValue: { [weak self] _ in
                guard let self = self else { return }
                servers.forEach { _ = self.storage.save(server: $0).sink { _ in} receiveValue: { _ in} }
            }
    }
}

