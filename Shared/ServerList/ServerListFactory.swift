import SwiftUI

public protocol ServerListFactoryProtocol {
    func make() -> ServerListView
}

public final class ServerListFactory: ServerListFactoryProtocol {
    private let authService: AuthServiceProtocol
    private let networkService: NetworkServiceProtocol
    private let storage: ServerResponseStorage
    
    init(
        authService: AuthServiceProtocol,
        networkService: NetworkServiceProtocol,
        storage: ServerResponseStorage
    ) {
        self.authService = authService
        self.networkService = networkService
        self.storage = storage
    }
    
    public func make() -> ServerListView {
        let viewModel = ServerListViewModel()
        let router = ServerListRouter()
        let interactor = ServerListInteractor(networkService: networkService, storage: storage)

        let presenter = ServerListPresenter(
            authService: authService,
            router: router,
            interactor: interactor,
            model: viewModel
        )
        let view = ServerListView(
            presenter: presenter,
            model: viewModel
        )
        presenter.view = view
        router.view = view
        return view
    }
}
