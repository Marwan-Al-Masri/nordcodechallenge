import SwiftUI
import CoreData
public protocol ServerListViewProtocol {}

public struct ServerListView: View, ServerListViewProtocol {
    private let presenter: ServerListPresenterProtocol
    @ObservedObject var model: ServerListViewModel
    
    public init(
        presenter: ServerListPresenterProtocol,
        model: ServerListViewModel
    ) {
        self.presenter = presenter
        self.model = model
        presenter.fetchServers()
    }
    
#if os(iOS)
    public var body: some View {
        NavigationView {
            List {
                ForEach(model.list ?? []) { server in
                    HStack() {
                        Text(server.name ?? "")
                        Spacer()
                        Text("\(server.distance) km")
                    }
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle("Tistio.")
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    
                    Button(action: filter) {
                        Label("Filter", image: "filter_icn")
                        Text("Filter")
                    }
                }
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Button(action: logout) {
                        Text("Logout")
                        Label("Logout", image: "logout_icn")
                    }
                }
            }
            .confirmationDialog("Sort", isPresented: $model.showActionSheet, titleVisibility: .hidden) {
                Button("By distance") {
                    filterByDistance()
                }

                Button("Alphabetical") {
                    filterAlphabetical()
                }
            }
        }
    }
#else
    public var body: some View {
        List {
            HStack() {
                Button(action: filterAlphabetical) {
                    Label("SERVER", image: "filter_icn")
                }
                Spacer()
                Button(action: filterByDistance) {
                    Label("DISTANCE", image: "filter_icn")
                }
            }
            ForEach(model.list ?? []) { server in
                HStack() {
                    Text(server.name ?? "")
                    Spacer()
                    Text("\(server.distance) km")
                }
                Rectangle()
                    .frame(height: 1)
                    .padding(.horizontal, 1)
                    .foregroundColor(.gray)
            }
        }
        .toolbar {
            Button(action: logout) {
                Text("Logout")
                Label("Logout", image: "logout_icn")
            }
        }
        .navigationTitle("Testio.")
    }
#endif
    
    private func logout() {
        presenter.logout()
    }
    
    private func filter() {
        model.showActionSheet = true
    }
    
    private func filterByDistance() {
        model.list = model.list?.sorted(by: { $0.distance <= $1.distance })
    }
    
    private func filterAlphabetical() {
        model.list = model.list?.sorted(by: { ($0.name ?? "") <= ($1.name ?? "") })
    }
}


//struct ServerListView_Previews: PreviewProvider {
//    static var previews: some View {
//        ServerListView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
//    }
//}
