import SwiftUI
import Combine

public protocol ServerListPresenterProtocol {
    func fetchServers()
    func logout()
}

final class ServerListPresenter: ServerListPresenterProtocol {
    var view: ServerListViewProtocol?
    @ObservedObject var model: ServerListViewModel
    
    private let authService: AuthServiceProtocol
    private let router: ServerListRouterProtocol
    private let interactor: ServerListInteractorProtocol
    
    private var task: AnyCancellable?

    init(
        authService: AuthServiceProtocol,
        router: ServerListRouterProtocol,
        interactor: ServerListInteractorProtocol,
        model: ServerListViewModel
    ) {
        self.authService = authService
        self.router = router
        self.interactor = interactor
        self.model = model
    }
}

// MARK: - ServerListPresenterProtocol

extension ServerListPresenter {
    
    func fetchServers() {
        guard let token = authService.token else {
            return
        }
        _ = interactor.loadServers()
            .sink { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let error):
                    self.model.error = error
                    debugPrint(error)
                case .finished: break
                }
            } receiveValue: { [weak self] list in
                guard let self = self else { return }
                self.model.list = list
            }
        
        task = interactor.fetchServers(token: token)
            .sink { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let error):
                    self.model.error = error
                    debugPrint(error)
                case .finished: break
                }
            } receiveValue: { [weak self] list in
                guard let self = self else { return }
                self.model.list = list
                self.interactor.replaceServers(list)
            }
    }
    
    func logout() {
        authService.token = nil
    }
}

// MARK: - Private Methods

extension ServerListPresenter {
}
