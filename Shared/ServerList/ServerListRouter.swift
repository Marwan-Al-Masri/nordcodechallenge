import Foundation

public protocol ServerListRouterProtocol {}

final class ServerListRouter: ServerListRouterProtocol {
    var view: ServerListViewProtocol?
}

// MARK: - ServerListRouterProtocol

extension ServerListRouter {}
