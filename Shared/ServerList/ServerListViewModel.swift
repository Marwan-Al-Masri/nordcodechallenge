import SwiftUI

public class ServerListViewModel: ObservableObject {
    
    @Published var list: [Server]?
    @Published var showActionSheet = false
    @Published var error: Error?
}
