import SwiftUI
import CoreData


struct ContentView: View {
    
    @EnvironmentObject var auth: AuthService
    
    var body: some View {
        if auth.isAuthenticated {
            DIContainer.default.serverListFactory.make()
        } else {
            DIContainer.default.authenticationFactory.make()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(DIContainer.default.authService.service)
    }
}
