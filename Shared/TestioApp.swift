import SwiftUI


@main
struct TestioApp: App {
    let service = DIContainer.default.authService.service
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(service)
        }
    }
}
