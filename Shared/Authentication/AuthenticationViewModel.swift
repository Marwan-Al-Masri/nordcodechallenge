import Foundation
import SwiftUI
import Combine

public class AuthenticationViewModel: ObservableObject {

    @Published var isLoading = false
    @Published var showAlert = false
    @Published var error: NetworkError?
}
