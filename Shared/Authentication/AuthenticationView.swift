import SwiftUI

public protocol AuthenticationViewProtocol {
    
}


public struct AuthenticationView: View, AuthenticationViewProtocol {
    private let presenter: AuthenticationPresenterProtocol
    @ObservedObject var model: AuthenticationViewModel
    
    public init(
        presenter: AuthenticationPresenterProtocol,
        model: AuthenticationViewModel
    ) {
        self.presenter = presenter
        self.model = model
    }
    
    // Binding
    @State var username: String = ""
    @State var password: String = ""
#if os(iOS)
    public var body: some View {
        
        LoadingView(isShowing: .constant(model.isLoading)) {
            
            ZStack {
                VStack(alignment: .center){
                    Spacer()
                    Image("ios_img")
                        .resizable()
                        .scaledToFit()
                }
                
                if !model.isLoading {
                    
                    GeometryReader{ geometry in
                        VStack(alignment: .center) {
                            Spacer()
                            Image("logo_img")
                                .resizable()
                                .frame(width: 186, height: 48)
                            
                            Spacer(minLength: 40)
                            CustomText(
                                iconSystemName: "person.crop.circle.fill",
                                placeholder: "Username",
                                isSecure: false,
                                text: $username
                            )
                                .frame(width: geometry.size.width - 64, height: 48)
                            Spacer(minLength: 16)
                            CustomText(
                                iconSystemName: "lock.circle.fill",
                                placeholder: "Password",
                                isSecure: true,
                                text: $password
                            )
                                .frame(width: geometry.size.width - 64, height: 48)
                            Spacer(minLength: 24)
                            Button(
                                action: { login() },
                                label:  {
                                    Text("Log in")
                                        .frame(width: geometry.size.width - 64, height: 40)
                                        .font(.title3)
                                        .foregroundColor(.white)
                                        .background(Color(red: 0.275, green: 0.529, blue: 1))
                                        .cornerRadius(10)
                                }
                            )
                                .frame(width: geometry.size.width - 64, height: 40)
                            Spacer(minLength: 200)
                        }
                        .padding(32)
                    }
                }
                
            }.background(Color.white)
        }
            .alert(isPresented: $model.showAlert, content: { () -> Alert in
                Alert(title: Text("Verification Failed"), message: Text("Your username or password is incorrect."), dismissButton: .default(Text("OK"), action: {
                    self.model.showAlert = false
                    self.model.error = nil
                }))
            })
    }
#else
    public var body: some View {
        GeometryReader{ geometry in
            HStack {
                VStack(alignment: .center) {
                    VStack {
                        Image("logo_img")
                            .resizable()
                            .frame(width: 200, height: 51)
                        
                        Spacer(minLength: 40)
                        CustomText(
                            iconSystemName: "person.crop.circle.fill",
                            placeholder: "Username",
                            isSecure: false,
                            text: $username
                        )
                            .frame(width: 200, height: 30)
                        Spacer(minLength: 24)
                        CustomText(
                            iconSystemName: "lock.circle.fill",
                            placeholder: "Password",
                            isSecure: true,
                            text: $password
                        )
                            .frame(width: 200, height: 30)
                        Spacer(minLength: 24)
                        Button(
                            action: { login() },
                            label:  {
                                Text("Log in")
                                    .frame(width: 200, height: 30)
                                    .font(.title3)
                                    .foregroundColor(.white)
                                    .background(Color(red: 0.275, green: 0.529, blue: 1))
                                    .cornerRadius(10)
                            }
                        )
                            .frame(width: 200, height: 30)
                    }
                    .frame(width: 200, height: 201)
                }
                .frame(width: geometry.size.width / 2, height: geometry.size.height)
                
                Image("macos_img")
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width / 2, height: geometry.size.height)
            }
            .frame(width: geometry.size.width, height: geometry.size.height)
        }
        .background(Color.white)
        .alert(isPresented: $model.showAlert, content: { () -> Alert in
            Alert(title: Text("Verification Failed"), message: Text("Your username or password is incorrect."), dismissButton: .default(Text("OK"), action: {
                self.model.showAlert = false
                self.model.error = nil
            }))
        })
    }
#endif

    func login() {
        debugPrint("Button tapped", username, password)
        presenter.login(username: username, password: password)
    }
}

//public struct AuthenticationView_Previews: PreviewProvider {
//    public static var previews: some View {
//        AuthenticationView()
//    }
//}
