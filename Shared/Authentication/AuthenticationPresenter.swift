import Foundation
import SwiftUI
import Combine

public protocol AuthenticationPresenterProtocol {
    func login(username: String, password: String)
}

final class AuthenticationPresenter: AuthenticationPresenterProtocol {
    var view: AuthenticationViewProtocol?
    @ObservedObject var model: AuthenticationViewModel
    
    private let authService: AuthServiceProtocol
    private let router: AuthenticationRouterProtocol
    private let interactor: AuthenticationInteractorProtocol
    
    private var task: AnyCancellable?

    init(
        router: AuthenticationRouterProtocol,
        interactor: AuthenticationInteractorProtocol,
        authService: AuthServiceProtocol,
        model: AuthenticationViewModel
    ) {
        self.router = router
        self.interactor = interactor
        self.authService = authService
        self.model = model
    }
}

// MARK: - AuthenticationPresenterProtocol

extension AuthenticationPresenter {
    
    func login(username: String, password: String) {
        model.isLoading = true
        task = interactor.authenticat(username: username, password: password)
            .sink { result in
                switch result {
                case .finished:
                    self.model.isLoading = false
                case .failure(let error):
                    debugPrint(error)
                    self.model.error = error
                    self.model.showAlert = true
                    self.model.isLoading = false
                }
            } receiveValue: { model in
                self.authService.token = model.token
            }
    }
}

// MARK: - Private Methods

extension AuthenticationPresenter {
}
