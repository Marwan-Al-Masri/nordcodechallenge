import Combine

public protocol AuthenticationInteractorProtocol {
    func authenticat(username: String, password: String) -> AnyPublisher<AuthModel, NetworkError>
}

final class AuthenticationInteractor: AuthenticationInteractorProtocol {
    private let networkService: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func authenticat(username: String, password: String) -> AnyPublisher<AuthModel, NetworkError> {
        networkService.authenticate(username: username, password: password)
            .eraseToAnyPublisher()
    }
}
