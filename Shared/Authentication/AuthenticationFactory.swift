import SwiftUI

public protocol AuthenticationFactoryProtocol {
    func make() -> AuthenticationView
}

public final class AuthenticationFactory: AuthenticationFactoryProtocol {
    private let authService: AuthServiceProtocol
    private let networkService: NetworkServiceProtocol

    init(
        authService: AuthServiceProtocol,
        networkService: NetworkServiceProtocol
    ) {
        self.authService = authService
        self.networkService = networkService
    }

    public func make() -> AuthenticationView {
        
        let viewModel = AuthenticationViewModel()
        let router = AuthenticationRouter()
        let interactor = AuthenticationInteractor(networkService: networkService)

        let presenter = AuthenticationPresenter(
            router: router,
            interactor: interactor,
            authService: authService,
            model: viewModel
        )
        let view = AuthenticationView(presenter: presenter, model: viewModel)
        presenter.view = view
        return view
    }
}
