import Foundation

public protocol AuthenticationRouterProtocol {}

final class AuthenticationRouter: AuthenticationRouterProtocol {
    var view: AuthenticationView?
}

// MARK: - AuthenticationRouterProtocol

extension AuthenticationRouter {}
