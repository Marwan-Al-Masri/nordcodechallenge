import SwiftUI

public struct CustomText : View{
    var iconSystemName: String
    var placeholder: String
    var isSecure: Bool
    @Binding var text: String
    
    let foregroundColor = Color(red: 0.235, green: 0.235, blue: 0.263, opacity: 0.60)
    let backgroundColor = Color(red: 0.46, green: 0.46, blue: 0.50, opacity: 0.12)
    let textColor = Color(red: 0.235, green: 0.235, blue: 0.263)
    
    public var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Image(systemName: iconSystemName)
                    .resizable()
                    .frame(width: 14, height: 14)
                    .foregroundColor(foregroundColor)
                
                if isSecure {
                    SecureField("", text: $text)
                        .background(Color.clear)
                        .foregroundColor(textColor)
#if os(iOS)
                        .autocapitalization(.none)
#endif
                        .placeholder(when: text.isEmpty) {
                            Text(placeholder).foregroundColor(foregroundColor)
                        }
                } else {
                    TextField("", text: $text)
                        .background(Color.clear)
                        .foregroundColor(textColor)
#if os(iOS)
                        .autocapitalization(.none)
#endif
                        .placeholder(when: text.isEmpty) {
                            Text(placeholder).foregroundColor(foregroundColor)
                        }
                }
                
                    
            }
        }
        .padding(10)
        .cornerRadius(20)
        .background(backgroundColor)
        .shadow(color: .gray, radius: 10)
    }
}

fileprivate extension View {
    func placeholder<Content: View>(
        when shouldShow: Bool,
        alignment: Alignment = .leading,
        @ViewBuilder placeholder: () -> Content) -> some View {

        ZStack(alignment: alignment) {
            placeholder().opacity(shouldShow ? 1 : 0)
            self
        }
    }
}
