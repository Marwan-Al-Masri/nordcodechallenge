import SwiftUI

public struct CustomButtonText: View {
    var title: String
    
    public var body: some View {
        Text(title)
            .font(.system(size: 17))
            .foregroundColor(.white)
            .padding()
            .background(Color(red: 0.275, green: 0.529, blue: 1))
            .cornerRadius(10)
    }
}
