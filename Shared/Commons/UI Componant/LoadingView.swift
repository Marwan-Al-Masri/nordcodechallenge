import SwiftUI

struct LoadingIndicator: UIViewRepresentable {

    typealias UIViewType = UIActivityIndicatorView

    let style: UIActivityIndicatorView.Style

    func makeUIView(context: UIViewRepresentableContext<LoadingIndicator>) -> LoadingIndicator.UIViewType {
        let indecator = UIActivityIndicatorView(style: style)
        indecator.color = UIColor(red: 0.24, green: 0.24, blue: 0.26, alpha: 1.0)
        return indecator
    }

    func updateUIView(_ view: LoadingIndicator.UIViewType, context: UIViewRepresentableContext<LoadingIndicator>) {
        view.startAnimating()
    }
    
}


struct LoadingView<Content>: View where Content: View {
    @Binding var isShowing: Bool
    var content: () -> Content
    var color = Color(red: 0.24, green: 0.24, blue: 0.26)

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                if (!self.isShowing) {
                    self.content()
                } else {
                    self.content()
                        .disabled(self.isShowing)

                    VStack {
                        LoadingIndicator(style: .medium)
                        Text("Loading List")
                    }
                    .frame(width: geometry.size.width, height: geometry.size.height)
                    .background(Color.clear)
                    .foregroundColor(color)
                    .opacity(self.isShowing ? 1 : 0)
                }
            }
        }
    }
}
