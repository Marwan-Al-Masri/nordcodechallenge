import Foundation
import Combine

final class DefaultAuthService: AuthServiceProtocol {
    
    private let key = "AUTH_TOKEN"
    
    private let userDefault = UserDefaults.standard
    
    var token: String? {
        get { userDefault.object(forKey: key) as? String }
        
        set {
            defer { service.isAuthenticated = isAuthenticated }
            if let newValue = newValue {
                userDefault.set(newValue, forKey: key)
            } else {
                userDefault.removeObject(forKey: key)
            }
        }
    }
    
    var isAuthenticated: Bool {
        token != nil
    }
    
    var service: AuthService = AuthService()
    
    init() {
        service = AuthService()
        service.setAuth(isAuthenticated: isAuthenticated)
    }
}
