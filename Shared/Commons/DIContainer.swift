import Foundation

class DIContainer {
    
    /// Returns the default `DIResolver`, singleton pattern
    /// - Note: this object is thread safe.
    public static var `default`: DIContainer {
        struct Singleton {
            static var `default` = DIContainer()
            static let syncQueue = DispatchQueue(label: "DIResolver.Singleton")
        }
        return Singleton.syncQueue.sync { Singleton.default }
    }
    
    public let authService: AuthServiceProtocol
    public let networkService: NetworkServiceProtocol
    public let storage: ServerResponseStorage
    public let authenticationFactory: AuthenticationFactoryProtocol
    public let serverListFactory: ServerListFactoryProtocol
    
    init(
        authService: AuthServiceProtocol = DefaultAuthService(),
        networkService: NetworkServiceProtocol = NetworkService(),
        storage: ServerResponseStorage = CoreDataServerResponseStorage(),
        authenticationFactory: AuthenticationFactoryProtocol? = nil,
        serverListFactory: ServerListFactoryProtocol? = nil
    ) {
        self.authService = authService
        self.networkService = networkService
        self.storage = storage
        if let factory = authenticationFactory {
            self.authenticationFactory = factory
        } else {
            self.authenticationFactory = AuthenticationFactory(authService: authService, networkService: networkService)
        }
        if let factory = serverListFactory {
            self.serverListFactory = factory
        } else {
            self.serverListFactory = ServerListFactory(authService: authService, networkService: networkService, storage: storage)
        }
    }
}
