import CoreData
import Combine

final class CoreDataServerResponseStorage {

    private let coreDataStorage: CoreDataStorage

    init(coreDataStorage: CoreDataStorage = CoreDataStorage.shared) {
        self.coreDataStorage = coreDataStorage
    }
    
    private func deleteAllObject(for entityName: String, in context: NSManagedObjectContext) -> Error? {
        
            let fetchItemsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            do {
                let objects = try context.fetch(fetchItemsRequest)
                for object in objects as! [NSManagedObject] {
                    context.delete(object)
                }
                try context.save()
                return nil
            } catch {
                return error
            }
    }
}

extension CoreDataServerResponseStorage: ServerResponseStorage {
    func save(server: Server) -> Future<Void, Error> {
        Future({ promise in
            self.coreDataStorage.performBackgroundTask { context in
                _ = server.toEntity(in: context)
                do {
                    try context.save()
                    promise(.success(()))
                } catch {
                    promise(.failure(error))
                }
            }
        })
    }
    
    func fetch() -> Future<[Server], Error> {
        Future({ promise in
            self.coreDataStorage.performBackgroundTask { context in
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ServerEntity")
                do {
                    let objects = try context.fetch(fetchRequest)
                    var result: [ServerEntity] = []
                    for object in objects as! [NSManagedObject] {
                        if let server = object as? ServerEntity {
                            result.append(server)
                        }
                    }
                    let list = result.map { Server(name: $0.name, distance: Int($0.distance)) }
                    promise(.success(list))
                } catch {
                    promise(.failure(error))
                }
            }
        })
    }
    
    func clear() -> Future<Void, Error>  {
        Future({ promise in
            self.coreDataStorage.performBackgroundTask { context in
                if let error = self.deleteAllObject(for: "ServerEntity", in: context) {
                    promise(.failure(error))
                } else {
                    promise(.success(()))
                }
            }
        })
    }
}
