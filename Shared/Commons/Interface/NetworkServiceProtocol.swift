import Foundation
import Combine
import Alamofire

public struct NetworkError: Codable, Error, CustomStringConvertible {
    public var statusCode: Int?
    public var message: String?
    
    public var description: String {
        message ?? "Please try again later"
    }
    
}

/**
 An object that responsible  for managing all connection and calls to the API.
 */
public protocol NetworkServiceProtocol {
    func authenticate(username: String, password: String ) -> Future<AuthModel, NetworkError>
    func fetchServers(token: String) -> Future<[Server], NetworkError>
}

extension NetworkServiceProtocol {
    public func request<T: Decodable, E: Error>(
        url: String,
        method: HTTPMethod,
        parameters: Parameters? = nil,
        decoder: JSONDecoder = JSONDecoder(),
        headers: HTTPHeaders? = []
    ) -> Future<T, E> {
        var defaultHeaders = HTTPHeaders.default
        for header in headers ?? [] {
            defaultHeaders.add(header)
        }
        return Future({ promise in
            AF.request(
                url,
                method: method,
                parameters: parameters,
                headers: defaultHeaders
            ).responseDecodable(decoder: decoder, completionHandler: { (response: DataResponse<T, AFError>) in
                switch response.result {
                    case .success(let value):
                        promise(.success(value))
                    case .failure(let error):
                        promise(
                            .failure(NetworkError(
                                statusCode: response.response?.statusCode,
                                message: error.localizedDescription
                            ) as! E)
                    )
                }
            })
        })
    }
}
