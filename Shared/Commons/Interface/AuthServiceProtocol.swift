import Foundation
import Combine

/**
 An object that responsible for user authentication.
 */
public protocol AuthServiceProtocol: AnyObject {
    
    /// Access tokens is used to allow an application to access an API.
    /// - warning: this object should saved in a persistent layer upon confirming to this protocol.
    var token: String? { get set }
    
    var service: AuthService { get }
}

public extension AuthServiceProtocol {
    var isAuthenticated: Bool { token != nil }
}

public class AuthService: ObservableObject {
    
    @Published var isAuthenticated: Bool = false

    func setAuth(isAuthenticated: Bool) {
        self.isAuthenticated = isAuthenticated
    }
}
