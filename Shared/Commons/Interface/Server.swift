import Foundation
import CoreData
import Alamofire

public struct AuthModel: Codable {
    public var token: String
}

public class Server: Codable {
    
    /// A `String` object represents the server such as (Germany #32).
    public var name: String?

    /// An `Int` value represents how far is the server from your current location.
    public var distance: Int
    
    public init(name: String?, distance: Int) {
        self.name = name
        self.distance = distance
    }
    
    func toEntity(in context: NSManagedObjectContext) -> ServerEntity {
        let entity: ServerEntity = .init(context: context)
        entity.name = name
        entity.distance = Int64(distance)
        return entity
    }
}
extension Server: Identifiable { }

