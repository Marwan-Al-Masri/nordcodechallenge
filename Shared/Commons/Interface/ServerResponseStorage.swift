import Combine

protocol ServerResponseStorage {
    func save(server: Server) -> Future<Void, Error>
    func fetch() -> Future<[Server], Error>
    func clear() -> Future<Void, Error>
}
