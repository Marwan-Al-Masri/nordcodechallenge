import Foundation
import Combine

final class NetworkService: NetworkServiceProtocol {
    let baseURL: String
    
    init(baseURL: String = "https://playground.tesonet.lt/v1/") {
        self.baseURL = baseURL
    }
    
    func authenticate(username: String, password: String ) -> Future<AuthModel, NetworkError> {
        request(
            url: baseURL + "tokens",
            method: .post,
            parameters: [
                "username": username,
                "password": password
            ]
        )
    }

    func fetchServers(token: String) -> Future<[Server], NetworkError> {
        request(
            url: baseURL + "servers",
            method: .get,
            headers: [
                "Authorization": token
            ]
        )
    }
}

extension NetworkService {
}
