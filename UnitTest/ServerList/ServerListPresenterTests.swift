import XCTest
@testable import Testio

final class ServerListPresenterTests: XCTestCase {
    var subjectUnderTest: ServerListPresenter!
    var authService: MockAuthService!
    var router: MockServerListRouter!
    var interactor: MockServerListInteractor!
    var model: ServerListViewModel!

    override func setUpWithError() throws {
        authService = MockAuthService()
        router = MockServerListRouter()
        interactor = MockServerListInteractor()
        model = ServerListViewModel()
        subjectUnderTest = ServerListPresenter(
            authService: authService,
            router: router,
            interactor: interactor,
            model: model
        )
    }
    
    func testLogout() throws {
        // When
        subjectUnderTest.logout()
        
        // Then
        XCTAssertNil(model.error)
        XCTAssertNil(authService.token)
    }
    
    func testFetchServers() throws {
        // Given
        interactor.stubbedFetchServersResult = .success([Server(name: "server", distance: 10)])
        
        // When
        subjectUnderTest.fetchServers()
        
        // Then
        XCTAssertNotNil(model.list)
        XCTAssertEqual(model.list?.count, 1)
        XCTAssertEqual(model.list?.first?.name, "server")
        XCTAssertEqual(model.list?.first?.distance, 10)
    }
}
