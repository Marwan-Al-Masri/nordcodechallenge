import Combine
import Foundation
@testable import Testio

enum MockServerListInteractorError: Error {
    case someError
}

final class MockServerListInteractor: ServerListInteractorProtocol {
    
    var didCallFetchServers = false
    var didCallFetchServersWithToken: String?
    var stubbedFetchServersResult: Result<[Server], NetworkError> = .failure(.init(statusCode: 404, message: "Host not found"))
    func fetchServers(token: String) -> AnyPublisher<[Server], NetworkError> {
        didCallFetchServers = true
        didCallFetchServersWithToken = token
        return Future { promise in
            promise(self.stubbedFetchServersResult)
        }.eraseToAnyPublisher()
    }
    
    var didCallLoadServers = false
    var stubbedLoadServersResult: Result<[Server], Error> = .failure(MockServerListInteractorError.someError)
    func loadServers() -> AnyPublisher<[Server], Error> {
        didCallLoadServers = true
        return Future { promise in
            promise(self.stubbedLoadServersResult)
        }.eraseToAnyPublisher()
    }
    
    var didCallReplaceServers = false
    var didCallReplaceServersWithServers: [Server]?
    func replaceServers(_ servers: [Server]) {
        didCallReplaceServers = true
    }
}
