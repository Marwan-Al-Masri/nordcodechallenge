import Foundation
@testable import Testio

final class MockServerListPresenter: ServerListPresenterProtocol {
    var didCallFetchServers = false
    func fetchServers() {
        didCallFetchServers = true
    }
    
    var didCallLogout = false
    func logout() {
        didCallLogout = true
    }
}
