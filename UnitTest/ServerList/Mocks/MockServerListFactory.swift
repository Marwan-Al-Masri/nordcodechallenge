import Foundation
@testable import Testio

final class MockServerListFactory: ServerListFactoryProtocol {
    
    let viewModel = ServerListViewModel()
    let presenter = MockServerListPresenter()
    var didCallMake = false
    let stubbedView: ServerListView

    init() {
        self.stubbedView = ServerListView(presenter: presenter, model: viewModel)
    }

    func make() -> ServerListView {
        didCallMake = true
        return stubbedView
    }
}
