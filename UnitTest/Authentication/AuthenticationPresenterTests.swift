import XCTest
@testable import Testio

final class AuthenticationPresenterTests: XCTestCase {
    var subjectUnderTest: AuthenticationPresenter!
    var router: MockAuthenticationRouter!
    var interactor: MockAuthenticationInteractor!
    var authService: MockAuthService!
    var model: AuthenticationViewModel!

    override func setUpWithError() throws {
        router = MockAuthenticationRouter()
        interactor = MockAuthenticationInteractor()
        authService = MockAuthService()
        model = AuthenticationViewModel()
        subjectUnderTest = AuthenticationPresenter(
            router: router,
            interactor: interactor,
            authService: authService,
            model: model
        )
    }
    
    func testLoginSuccess() throws {
        // Given
        let username = "username", password = "password"
        interactor.stubbedAuthenticateResult = .success(AuthModel.init(token: "123"))
        
        // When
        subjectUnderTest.login(username: username, password: password)
        
        // Then
        XCTAssertNil(model.error)
        XCTAssertFalse(model.isLoading)
        XCTAssertFalse(model.showAlert)
        XCTAssertNotNil(authService.token)
        XCTAssertEqual(authService.token, "123")
    }
    
    func testLoginFail() throws {
        // Given
        let username = "username", password = "password"
        
        // When
        subjectUnderTest.login(username: username, password: password)
        
        // Then
        XCTAssertNotNil(subjectUnderTest.model.error)
        XCTAssertEqual(subjectUnderTest.model.error?.statusCode, 404)
        XCTAssertFalse(subjectUnderTest.model.isLoading)
        XCTAssertTrue(subjectUnderTest.model.showAlert)
        XCTAssertNil(authService.token)
    }
}
