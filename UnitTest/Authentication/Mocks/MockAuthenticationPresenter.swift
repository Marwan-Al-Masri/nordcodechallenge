import Foundation
@testable import Testio

final class MockAuthenticationPresenter: AuthenticationPresenterProtocol {
    var didCallLogin = false
    var didCallLoginWithUsername: String?
    var didCallLoginWithPassword: String?
    func login(username: String, password: String) {
        didCallLogin = true
        didCallLoginWithUsername = username
        didCallLoginWithPassword = password
    }
}
