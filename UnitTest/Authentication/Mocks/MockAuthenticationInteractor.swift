import Combine
import Foundation
@testable import Testio

public final class MockAuthenticationInteractor: AuthenticationInteractorProtocol {
    var didCallAuthenticate = false
    var didCallAuthenticateWithUsername: String?
    var didCallAuthenticateWithPassword: String?
    var stubbedAuthenticateResult: Result<AuthModel, NetworkError> = .failure(.init(statusCode: 404, message: "Host not found"))
    public func authenticat(username: String, password: String) -> AnyPublisher<AuthModel, NetworkError> {
        didCallAuthenticate = true
        didCallAuthenticateWithUsername = username
        didCallAuthenticateWithPassword = password
        return Future { promise in
            promise(self.stubbedAuthenticateResult)
        }.eraseToAnyPublisher()
    }
}
