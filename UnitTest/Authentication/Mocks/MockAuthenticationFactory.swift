import Foundation
@testable import Testio

final class MockAuthenticationFactory: AuthenticationFactoryProtocol {
    let viewModel = AuthenticationViewModel()
    let presenter = MockAuthenticationPresenter()
    var didCallMake = false
    let stubbedView: AuthenticationView

    init() {
        let view = AuthenticationView(presenter: presenter, model: viewModel)
        self.stubbedView = view
    }

    func make() -> AuthenticationView {
        didCallMake = true
        return stubbedView
    }
}
