import Foundation
import Combine
@testable import Testio

final class MockNetworkService: NetworkServiceProtocol {
    var didCallAuthenticate = false
    var didCallAuthenticateWithUsername: String?
    var didCallAuthenticateWithPassword: String?
    var stubbedAuthenticateResult: Result<AuthModel, NetworkError> = .failure(.init(statusCode: 404, message: "Host not found"))
    func authenticate(username: String, password: String) -> Future<AuthModel, NetworkError> {
        didCallAuthenticate = true
        didCallAuthenticateWithUsername = username
        didCallAuthenticateWithPassword = password
        return Future { promise in
            promise(self.stubbedAuthenticateResult)
        }
    }
    
    var didCallFetchServers = false
    var didCallFetchServersWithToken: String?
    var stubbedFetchServersResult: Result<[Server], NetworkError> = .failure(.init(statusCode: 404, message: "Host not found"))
    func fetchServers(token: String) -> Future<[Server], NetworkError> {
        didCallFetchServers = true
        didCallFetchServersWithToken = token
        return Future { promise in
            promise(self.stubbedFetchServersResult)
        }
    }
}
