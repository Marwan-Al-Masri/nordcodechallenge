import Foundation
@testable import Testio

final class MockAuthService: AuthServiceProtocol {
    var service: AuthService = AuthService()
    
    var didSetTokenCounter = 0
    var token: String? {
        didSet { didSetTokenCounter += 1 }
    }
}
